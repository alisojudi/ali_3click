﻿namespace ThreeClick
{
    class Program
    {   
        static int calculateArea(int [] input_arr)
        {   
            int area = 0;
            int max = 0;
            int maxValue = input_arr.Max();
            int countMaxValue = input_arr.Count(n => n==maxValue);
            int counterMax = 0;
            int maxIndex = 0;
            int length = input_arr.Length;

            for (int i=0 ;i< length; i++) {
                var num = input_arr[i];

                if(num>max){
                    max = num;
                }
                if(num==maxValue){
                    counterMax += 1;
                }

                if(counterMax == countMaxValue){
                    maxIndex = i+1;
                    break;
                }else{
                    area += max - num;
                }
            }

            Console.WriteLine("===========================");
            Console.WriteLine("REVERSE");
            
            Array.Reverse(input_arr);
            max = 0;
            
            for (int i=0 ;i< length-maxIndex; i++) {
                var num = input_arr[i];
                if(num>max){
                    max = num;
                }
                area += max - num;
            }
            Console.WriteLine("===========================");
            return area;
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Enter Array : ");
            try
            {
                var getInput = Console.ReadLine();
                if (getInput != null)
                {
                    var numbers = Array.ConvertAll(getInput.Split(','), int.Parse);
                    var getArea = calculateArea(numbers);
                    Console.WriteLine("Area is " + getArea.ToString());
                }
                else
                {
                    Console.WriteLine("Please enter the correct values.");
                    Environment.Exit(0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Message: {0}", e.Message);
                Environment.Exit(0);
            }

        }
    }
}